#include "z0_alloc.h"

#include <unistd.h>

void*
z0_alloc(uint32_t size) {
        static z0_meta *head = NULL;

        void *ptr;
        if (UNLIKELY(head == NULL)) {               
                ptr = sbrk(sizeof (z0_meta) + size);
                if (UNLIKELY(ptr == (void*)-1))
                        return NULL;

                head = ptr;
                head->is_free = false;
                head->size    = size;
                head->next    = NULL;
        } else {
                z0_meta *temp;
                z0_meta *tail;

                temp = head;
                tail = NULL;

                while (temp != NULL) {
                        if (temp->is_free && temp->size >= size)
                                break;
                 
                        tail = temp;
                        temp = temp->next;
                }

                ptr = temp;      
                if (ptr == NULL) 
                        ptr = sbrk(sizeof (z0_meta) + size);

                tail->next = (z0_meta*)ptr;
                tail->next->is_free = false;
                tail->next->size    = size;
                tail->next->next    = NULL;
        }

        ptr = (uint8_t*)ptr + sizeof (z0_meta);
        return ptr;
}

uint32_t
z0_size(void *ptr) {
        z0_meta *chunk;
        chunk = (z0_meta*)((uint8_t*)ptr - sizeof (z0_meta));

        return chunk->size;
}

void
z0_free_fast(void *ptr) {
        z0_meta *chunk;
        chunk = (z0_meta*)((uint8_t*)ptr - sizeof (z0_meta));

        chunk->is_free = true;
}

void
z0_free(void *ptr) {
        z0_meta *chunk;
        chunk = (z0_meta*)((uint8_t*)ptr - sizeof (z0_meta));

        if (LIKELY(chunk->next != NULL))
                chunk->is_free = true;
        else
                sbrk(-(sizeof (z0_meta) + chunk->size));
}
