# 0xALLOC

A simple custom memory allocator written in `C` using `sbrk`.

### API

- Allocating a chunk of memory:

```c
int *array;
array = z0_alloc(4 * sizeof (int));

array[0] = 0xA;
array[1] = 0xB;
array[2] = 0xC;
array[3] = 0xD;

printf("INFO: %i\n", array[0]);
printf("INFO: %i\n", array[1]);
printf("INFO: %i\n", array[2]);
printf("INFO: %i\n", array[3]);
```

- Freeing th memory (the faster approach). This does not actually free memory. Instead it marks the allocated chunk as available for later use.

```c
z0_free_fast(array);
```

- Freeing the memory (the default approach). Unlike the example from above, it tries to actually free the memory. However it will not free if the chunk is in between *the first* and *the last* chunks.

```c
z0_free(array);
```

- Getting the size of allocated chunk. The allocator stores extra meta information about the allocated chunk. You can get that information by using the following function:

```c
uint32_t arr_len;
arr_len = z0_size(array);

printf("INFO: %u\n", arr_len);
```
