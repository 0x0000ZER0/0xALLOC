#ifndef Z0_ALLOC_H
#define Z0_ALLOC_H

#include <stdint.h>
#include <stdbool.h>

#define LIKELY(f)   __builtin_expect((f), 1)
#define UNLIKELY(f) __builtin_expect((f), 0)

typedef struct z0_meta z0_meta;
struct z0_meta {  
        bool      is_free;
        uint32_t  size;
        z0_meta  *next;
};

void*
z0_alloc(uint32_t);

uint32_t
z0_size(void*);

void
z0_free_fast(void*);

void
z0_free(void*);

#endif
